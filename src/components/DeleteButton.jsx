import React from 'react';
import { Button, Dialog, DialogActions, DialogTitle, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

class DeleteButton extends React.Component {
  state = {
    open: false,
  };
  
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleAgree = () => {
    this.setState({ open: false });
    this.props.handleClick();
  }
  render() {
    return (
      <div>
          <IconButton onClick={this.handleClickOpen}>
                <DeleteIcon />
            </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Are you sure to delete this item?"}</DialogTitle>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Disagree
            </Button>
            <Button onClick={this.handleAgree} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DeleteButton;
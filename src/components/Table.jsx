import React from "react";
import { Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import { EditItem } from "./Edit";
import { Tbody } from "./Tbody";
class DrawTable extends React.Component {
  state = {
    clickIndex: "",
  };
  handleClick = index => this.setState({clickIndex: index })

  render() {
    const { itemsArray, removeItem, editItem } = this.props;
    return (
        <Table style={{ width: "60%", margin: "auto" }}>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Gross</TableCell>
              <TableCell>VAT%</TableCell>
              <TableCell>NET</TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {itemsArray.map((item, index) => {
              if (index !== this.state.clickIndex) {
                return (
                    <Tbody 
                      item = {item}
                      index = {index}
                      removeItem={item => removeItem(item)}
                      handleClick={i => this.handleClick(i)}
                    />
                );
              } else {
                return (
                  <TableRow key={index}>
                    <EditItem
                      item={item}
                      removeItem={item => removeItem(item)}
                      handleClick={i => this.handleClick(i)}
                      editItem={item => editItem(item)}
                    />
                  </TableRow>
                );
              }
            })}
          </TableBody>
        </Table>
    );
  }
}
export { DrawTable };

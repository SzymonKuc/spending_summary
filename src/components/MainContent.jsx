import React from "react";
import { Month } from "./Month";
import { ListContainer } from "./ListContainer";
import { filter, map } from "lodash";
import { Amount } from "./Amount";
import moment from 'moment';

class MainContent extends React.Component {
  constructor() {
    super();
    this.state = {
      listOfItems: [],
      filterListOfItems: [],
      date: moment().format("MMM YYYY"),
      id: '',
      amountObj: { amountOfGross: 0, amountOfNet: 0 }
    };

    this.addItemToArray = this.addItemToArray.bind(this);
    this.filterItemsByDate = this.filterItemsByDate.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  setDate(date) {
    this.setState({ date });
    this.filterItemsByDate(date);
  }
  filterItemsByDate(date) {
    const { listOfItems } = this.state;

    const newArray = filter(listOfItems, { date });

    this.setState({ filterListOfItems: newArray });
    this.amount(newArray);
  }
  addItemToArray(item, id) {
    this.setState(prevState => ({
      ...prevState,
      listOfItems: prevState.listOfItems.concat(item),
      filterListOfItems: prevState.filterListOfItems.concat(item),
      id: id
    }));
  }
  removeItem = item => {
    this.setState(prevState => ({
      ...prevState,
      amountObj: { amountOfGross: prevState.amountObj.amountOfGross - item.Gross, amountOfNet: prevState.amountObj.amountOfNet - item.Net },
      listOfItems: filter(prevState.listOfItems, el => el.id !== item.id),
      filterListOfItems: filter(prevState.filterListOfItems, el => el.id !== item.id)
  }));
}

  editItem = (item) => {

    const { filterListOfItems, listOfItems } = this.state;

    const filterList = this.returnEdit(filterListOfItems, item);
    const mainList = this.returnEdit(listOfItems, item);
    this.setState({ filterListOfItems: filterList, listOfItems: mainList });
    this.amount(filterList);
  };

  returnEdit = (items, item) => map(items, el => el.id === item.id ? item : el)

  amount = (items) => {
    const amountObj = { amountOfGross: 0, amountOfNet: 0 };

    items.forEach((el) => {
      amountObj.amountOfGross += el.Gross;
      amountObj.amountOfNet += el.Net;
    });

    this.setState({ amountObj });
  }

  render() {
    const { filterListOfItems, date} = this.state;
    return (
      <>
        <Month date={date => this.setDate(date)} dateNow={date} />
        <ListContainer
          filterListOfItems={filterListOfItems}
          date={date}
          addItem={(item, id) => this.addItemToArray(item, id)}
          removeItem={item => this.removeItem(item)}
          editItem={item => this.editItem(item)}
        />
        <Amount amount={this.state.amountObj} />
      </>
    );
  }
}

export { MainContent };

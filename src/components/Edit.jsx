import React from "react";
import { IconButton, TableCell, TextField } from "@material-ui/core";
import Edit from "@material-ui/icons/Edit";

class EditItem extends React.Component {
  state = {
    Name: "",
    Gross: 0,
    Vat: this.props.item.Vat,
    Net: 0,
    date: this.props.item.date,
    id: this.props.item.id
  };

  componentDidMount() {
    const { item } = this.props;
    this.setState({ Name: item.Name, Gross: item.Gross, Vat: item.Vat, Net: item.Net })
  }
  handleChange = event => {
    const { name, value } = event.target;
    const { Gross, Vat } = this.state;

    let netto = 0;
    if (name === "Vat" && !isNaN(value) && !isNaN(Gross)) {
       netto = (1 - value / 100) * Gross;
     } else if (name === "Gross" && Vat !== 0) {
       netto = (1 - Vat / 100) * value;
     }
   
   this.setState(prevState => ({
      [name]: name === "Name" ? value : parseInt(value),
      Net: netto === 0 ? prevState.Net : netto,
   }))

  };
  handleClick = () => {
    const { editItem, handleClick } = this.props;
    const { Gross, Vat, Net } = this.state;
    if (!isNaN(Gross) && !isNaN(Vat) && !isNaN(Net)) {
      editItem(this.state);
      handleClick(-1);
    }
    else {
      alert('Please insert a number!');
    }
  };
  render() {
    const { item } = this.props;
    return (
      <>
        <TableCell>
          <TextField placeholder="Name" label="Name" onChange={this.handleChange} name="Name" margin="dense" variant="outlined" defaultValue={item.Name} />
        </TableCell>
        <TableCell>
          <TextField label="Gross $" onChange={this.handleChange} name="Gross" margin="dense" variant="outlined" defaultValue={item.Gross} />
        </TableCell>
        <TableCell>
          <TextField label="VAT %" onChange={this.handleChange} name="Vat" margin="dense" variant="outlined" defaultValue={item.Vat} />
        </TableCell>
        <TableCell>
          <TextField label="Net $" onChange={this.handleChange} name="Net" margin="dense" variant="outlined" value={this.state.Net} />
        </TableCell>
        <TableCell>
        </TableCell>
        <TableCell>
          <IconButton onClick={this.handleClick}>
            <Edit />
          </IconButton>
        </TableCell>
      </>
    );
  }
}

export { EditItem };

import React from "react";
import { TableCell, TableRow, IconButton } from "@material-ui/core";
import Edit from "@material-ui/icons/Edit";
import DeleteButton from "./DeleteButton";

const Tbody = ({item, index, removeItem, handleClick}) => {
    return (
        <TableRow key={index}>
            <TableCell>{item.Name}</TableCell>
            <TableCell>{item.Gross}</TableCell>
            <TableCell>{item.Vat}</TableCell>
            <TableCell>{item.Net}</TableCell>
            <TableCell>
            <DeleteButton handleClick={() => removeItem(item)} />
            </TableCell>
            <TableCell>
            <IconButton onClick={() => handleClick(index)}>
                <Edit />
            </IconButton>
            </TableCell>
        </TableRow>
    );
}

export { Tbody };